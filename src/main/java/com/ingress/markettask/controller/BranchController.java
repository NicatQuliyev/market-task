package com.ingress.markettask.controller;

import com.ingress.markettask.dto.BranchRequest;
import com.ingress.markettask.dto.BranchResponse;
import com.ingress.markettask.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/branches")
@RequiredArgsConstructor
public class BranchController {

    private final BranchService branchService;

    @GetMapping
    public ResponseEntity<List<BranchResponse>> findAll() {
        return new ResponseEntity<>(branchService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{branchId}")
    public ResponseEntity<BranchResponse> findById(@PathVariable Long branchId) {
        return new ResponseEntity<>(branchService.findById(branchId), HttpStatus.OK);
    }

    @PostMapping("/market/{marketId}")
    public ResponseEntity<BranchResponse> save(@RequestBody BranchRequest request,
                                               @PathVariable Long marketId) {
        return new ResponseEntity<>(branchService.save(request, marketId), HttpStatus.CREATED);
    }

    @PutMapping("/{branchId}")
    public ResponseEntity<BranchResponse> update(@RequestBody BranchRequest request,
                                                 @PathVariable Long branchId) {
        return new ResponseEntity<>(branchService.update(request, branchId), HttpStatus.OK);
    }

    @DeleteMapping("/{branchId}")
    public void delete(@PathVariable Long branchId) {
        branchService.delete(branchId);
    }
}
