package com.ingress.markettask.controller;

import com.ingress.markettask.dto.MarketRequest;
import com.ingress.markettask.dto.MarketResponse;
import com.ingress.markettask.model.Market;
import com.ingress.markettask.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/markets")
@RequiredArgsConstructor
public class MarketController {

    private final MarketService marketService;

    @GetMapping
    public ResponseEntity<List<MarketResponse>> findAll() {
        return new ResponseEntity<>(marketService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MarketResponse> findById(@PathVariable Long id) {
        return new ResponseEntity<>(marketService.findById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<MarketResponse> create(@RequestBody MarketRequest request) {
        return new ResponseEntity<>(marketService.save(request), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MarketResponse> update(@PathVariable Long id,
                                                 @RequestBody MarketRequest request) {
        return new ResponseEntity<>(marketService.update(id, request), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        marketService.delete(id);
    }

    @PostMapping("/{marketId}/regions/{regionId}")
    public ResponseEntity<Market> addMarketToRegion(@PathVariable Long marketId, @PathVariable Long regionId) {
        return new ResponseEntity<>(marketService.addMarketToRegion(marketId, regionId), HttpStatus.CREATED);
    }
}
