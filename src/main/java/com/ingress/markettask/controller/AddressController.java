package com.ingress.markettask.controller;

import com.ingress.markettask.dto.AddressRequest;
import com.ingress.markettask.dto.AddressResponse;
import com.ingress.markettask.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/addresses")
@RequiredArgsConstructor
public class AddressController {
    private final AddressService addressService;

    @GetMapping
    public ResponseEntity<List<AddressResponse>> findAll() {
        return new ResponseEntity<>(addressService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{addressId}")
    public ResponseEntity<AddressResponse> findById(@PathVariable Long addressId) {
        return new ResponseEntity<>(addressService.findById(addressId), HttpStatus.OK);
    }

    @PostMapping("/branch/{branchId}")
    public ResponseEntity<AddressResponse> create(@RequestBody AddressRequest request, @PathVariable Long branchId) {
        return new ResponseEntity<>(addressService.save(request, branchId), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AddressResponse> update(@PathVariable Long id,
                                                  @RequestBody AddressRequest request) {
        return new ResponseEntity<>(addressService.update(id, request), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        addressService.delete(id);
    }
}
