package com.ingress.markettask.controller;

import com.ingress.markettask.dto.RegionRequest;
import com.ingress.markettask.dto.RegionResponse;
import com.ingress.markettask.service.RegionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/regions")
@RequiredArgsConstructor
public class RegionController {

    private final RegionService regionService;


    @GetMapping
    public ResponseEntity<List<RegionResponse>> findAll() {
        return new ResponseEntity<>(regionService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{regionId}")
    public ResponseEntity<RegionResponse> findById(@PathVariable Long regionId) {
        return new ResponseEntity<>(regionService.findById(regionId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<RegionResponse> save(@RequestBody RegionRequest request) {
        return new ResponseEntity<>(regionService.save(request), HttpStatus.CREATED);
    }

    @PutMapping("/{regionId}")
    public ResponseEntity<RegionResponse> update(@RequestBody RegionRequest request, @PathVariable Long regionId) {
        return new ResponseEntity<>(regionService.update(regionId, request), HttpStatus.OK);
    }

    @DeleteMapping("/{regionId}")
    public void delete(@PathVariable Long regionId) {
        regionService.delete(regionId);
    }
}
