package com.ingress.markettask.dto;

import com.ingress.markettask.model.Address;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class BranchResponse {

    Long id;

    String name;
    String countOfEmployee;
    Address address;
}
