package com.ingress.markettask.dto;

import com.ingress.markettask.model.Branch;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class AddressResponse {

    Long id;

    String name;

//    Branch branch;
}
