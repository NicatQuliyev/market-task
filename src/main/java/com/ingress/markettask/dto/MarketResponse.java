package com.ingress.markettask.dto;

import com.ingress.markettask.model.Region;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class MarketResponse {
    Long id;

    String name;

    String description;

    String type;

    List<BranchResponse> branches = new ArrayList<>();

    Set<Region> regions = new HashSet<>();
}
