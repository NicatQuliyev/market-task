package com.ingress.markettask;

import com.ingress.markettask.model.Branch;
import com.ingress.markettask.model.Market;
import com.ingress.markettask.model.Region;
import com.ingress.markettask.repository.BranchRepository;
import com.ingress.markettask.repository.MarketRepository;
import com.ingress.markettask.repository.RegionRepository;
import com.ingress.markettask.service.MarketService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class MarketTaskApplication implements CommandLineRunner {

    private final BranchRepository branchRepository;

    private final MarketRepository marketRepository;

    private final RegionRepository regionRepository;
    private final EntityManagerFactory entityManagerFactory;

    private final MarketService marketService;

    public static void main(String[] args) {
        SpringApplication.run(MarketTaskApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

//        EntityManager = entityManagerFactory.createEntityManager();

//        List<Branch> branches = entityManager.createQuery("select b from Branch b where b.market.id = :id",Branch.class)
//                .setParameter("id",2)
//                .getResultList();
//
//        List<Branch> all = branchRepository.getBranch(2L);
//        all.forEach(System.out::println);

//        System.out.println(branches);

        marketRepository.findAll().forEach(System.out::println);


//        marketService.addMarketToRegion(2L, 1L);
    }
}
