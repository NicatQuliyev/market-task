package com.ingress.markettask.repository;

import com.ingress.markettask.model.Market;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface MarketRepository extends JpaRepository<Market, Long> {
    @Override
//    @Query(value = "select m from Market m join fetch m.branches b join fetch b.address a")
    @EntityGraph(attributePaths = {"branches", "regions"}, type = EntityGraph.EntityGraphType.FETCH)
    List<Market> findAll();

    @Query(value = "select m from Market m join fetch m.regions r where r.id = :id ")
    Collection<Market> findAllByRegionId(Long id);

}