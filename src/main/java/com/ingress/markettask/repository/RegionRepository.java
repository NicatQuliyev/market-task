package com.ingress.markettask.repository;

import com.ingress.markettask.model.Region;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionRepository extends JpaRepository<Region, Long> {
}
