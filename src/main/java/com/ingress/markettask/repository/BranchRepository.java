package com.ingress.markettask.repository;

import com.ingress.markettask.model.Branch;
import com.ingress.markettask.model.Market;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BranchRepository extends JpaRepository<Branch, Long> {

    //    @Query(value = "select b from Branch b join fetch Address a where b.market.id = :id")
//    @Query(name = "Branch.findAllBranchesByMarket_id")
//    List<Branch> getBranch(Long id);

}
