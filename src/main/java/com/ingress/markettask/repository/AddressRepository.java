package com.ingress.markettask.repository;

import com.ingress.markettask.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
