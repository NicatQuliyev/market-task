package com.ingress.markettask.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "branch")
//@NamedQueries({
//        @NamedQuery(name = "Branch.findAllBranchesByMarket_id",query = "select b from Branch b where b.market.id = :id")
//})
public class Branch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;
    String countOfEmployee;

    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    Market market;

    @OneToOne(mappedBy = "branch")
    Address address;


}