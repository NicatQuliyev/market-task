package com.ingress.markettask.service;

import com.ingress.markettask.dto.RegionRequest;
import com.ingress.markettask.dto.RegionResponse;
import com.ingress.markettask.model.Region;
import com.ingress.markettask.repository.MarketRepository;
import com.ingress.markettask.repository.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RegionService {
    private final RegionRepository regionRepository;

    private final MarketRepository marketRepository;

    private final ModelMapper modelMapper;


    public List<RegionResponse> findAll() {
        return regionRepository
                .findAll()
                .stream()
                .map(region -> modelMapper.map(region, RegionResponse.class))
                .collect(Collectors.toList());
    }

    public RegionResponse findById(Long regionId) {
        Region region = regionRepository.findById(regionId).orElseThrow(() -> new RuntimeException(
                String.format("Region not found by id -%s", regionId)
        ));

        RegionResponse regionResponse = modelMapper.map(region, RegionResponse.class);

        return regionResponse;
    }

    public RegionResponse save(RegionRequest request) {
        Region region = modelMapper.map(request, Region.class);

        return modelMapper.map(regionRepository.save(region), RegionResponse.class);
    }

    public RegionResponse update(Long regionId, RegionRequest request) {
        Region region = regionRepository.findById(regionId).orElseThrow(() -> new RuntimeException(
                String.format("Region not found by id -%s", regionId)
        ));

        modelMapper.map(request, region, "map");
        region.setId(regionId);

        return modelMapper.map(regionRepository.save(region), RegionResponse.class);
    }

    public void delete(Long regionId) {
        var markets = marketRepository.findAllByRegionId(regionId);
        var region = regionRepository.findById(regionId).orElseThrow(() -> new RuntimeException(
                String.format("Region not found by id -%s", regionId)
        ));

        markets.forEach(market -> {
            market.getRegions().remove(region);
        });

        marketRepository.saveAll(markets);

        regionRepository.delete(region);
    }
}


