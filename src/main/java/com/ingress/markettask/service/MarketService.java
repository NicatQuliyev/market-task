package com.ingress.markettask.service;

import com.ingress.markettask.dto.MarketRequest;
import com.ingress.markettask.dto.MarketResponse;
import com.ingress.markettask.model.Market;
import com.ingress.markettask.model.Region;
import com.ingress.markettask.repository.BranchRepository;
import com.ingress.markettask.repository.MarketRepository;
import com.ingress.markettask.repository.RegionRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MarketService {

    private final MarketRepository marketRepository;

    private final BranchRepository branchRepository;

    private final RegionRepository regionRepository;

    private final ModelMapper modelMapper;


    public List<MarketResponse> findAll() {

        return marketRepository
                .findAll()
                .stream()
                .map(market -> modelMapper.map(market, MarketResponse.class))
                .collect(Collectors.toList());
    }

    public MarketResponse findById(Long id) {
        Market market = marketRepository.findById(id).orElseThrow(() -> new RuntimeException(
                String.format("Market not found with id %s", id)
        ));

        MarketResponse marketResponse = modelMapper.map(market, MarketResponse.class);

        return marketResponse;
    }

    public MarketResponse save(MarketRequest request) {
        Market market = modelMapper.map(request, Market.class);

        return modelMapper.map(marketRepository.save(market), MarketResponse.class);
    }

    public MarketResponse update(Long id, MarketRequest request) {
        Market market = marketRepository.findById(id).orElseThrow(() -> new RuntimeException(
                String.format("Market not found with id %s", id)
        ));

        modelMapper.map(request, market, "map");
        market.setId(id);

        return modelMapper.map(marketRepository.save(market), MarketResponse.class);
    }

    public void delete(Long id) {
        Market market = marketRepository.findById(id).orElseThrow(() -> new RuntimeException(
                String.format("Market not found with id %s", id)
        ));

        marketRepository.delete(market);
    }

    @Transactional
    public Market addMarketToRegion(Long marketId, Long regionId) {
        Region region = regionRepository.findById(regionId).orElseThrow(() -> new RuntimeException(
                String.format("Region not found with id -%s", regionId)
        ));

        Market market = marketRepository.findById(marketId).orElseThrow(() -> new RuntimeException(
                String.format("Market not found with id -%s", marketId)
        ));

        market.getRegions().add(region);

        marketRepository.save(market);

        return market;
    }
}
