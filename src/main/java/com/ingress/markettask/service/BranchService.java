package com.ingress.markettask.service;

import com.ingress.markettask.dto.BranchRequest;
import com.ingress.markettask.dto.BranchResponse;
import com.ingress.markettask.model.Address;
import com.ingress.markettask.model.Branch;
import com.ingress.markettask.model.Market;
import com.ingress.markettask.repository.AddressRepository;
import com.ingress.markettask.repository.BranchRepository;
import com.ingress.markettask.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BranchService {

    private final BranchRepository branchRepository;

    private final ModelMapper modelMapper;

    private final MarketRepository marketRepository;

    private final AddressRepository addressRepository;

    public List<BranchResponse> findAll() {
        return branchRepository
                .findAll()
                .stream()
                .map(branch -> modelMapper.map(branch, BranchResponse.class))
                .collect(Collectors.toList());
    }

    public BranchResponse findById(Long branchId) {
        Branch branch = branchRepository.findById(branchId).orElseThrow(() -> new RuntimeException(
                String.format("Branch not found with id -%s", branchId)
        ));

        BranchResponse branchResponse = modelMapper.map(branch, BranchResponse.class);

        return branchResponse;
    }


    public BranchResponse save(BranchRequest branchRequest, Long marketId) {
        Market market = marketRepository.findById(marketId).orElseThrow(() -> new RuntimeException(
                String.format("market not found with id -%s", marketId)
        ));

        Branch branch = modelMapper.map(branchRequest, Branch.class);
        branch.setMarket(market);

        return modelMapper.map(branchRepository.save(branch), BranchResponse.class);
    }


    public BranchResponse update(BranchRequest request, Long branchId) {
        Branch branch = branchRepository.findById(branchId).orElseThrow(() -> new RuntimeException(
                String.format("Branch not found with id -%s", branchId)
        ));

        modelMapper.map(request,branch,"map");
        branch.setId(branchId);

        return modelMapper.map(branchRepository.save(branch), BranchResponse.class);
    }

    public void delete(Long id) {
        Branch branch = branchRepository.findById(id).orElseThrow(() -> new RuntimeException(
                String.format("Branch not found with id -%s", id)
        ));


        branchRepository.delete(branch);
    }
}
