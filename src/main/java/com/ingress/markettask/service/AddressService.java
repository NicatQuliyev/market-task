package com.ingress.markettask.service;

import com.ingress.markettask.dto.AddressRequest;
import com.ingress.markettask.dto.AddressResponse;
import com.ingress.markettask.model.Address;
import com.ingress.markettask.model.Branch;
import com.ingress.markettask.model.Market;
import com.ingress.markettask.repository.AddressRepository;
import com.ingress.markettask.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AddressService {

    private final AddressRepository addressRepository;

    private final BranchRepository branchRepository;
    private final ModelMapper modelMapper;


    public List<AddressResponse> findAll() {
        return addressRepository
                .findAll()
                .stream()
                .map(address -> modelMapper.map(address, AddressResponse.class))
                .collect(Collectors.toList());
    }

    public AddressResponse findById(Long id) {
        Address address = addressRepository.findById(id).orElseThrow(() -> new RuntimeException(
                String.format("Address not found with id %s", id)
        ));

        AddressResponse addressResponse = modelMapper.map(address, AddressResponse.class);


        return addressResponse;
    }

    public AddressResponse save(AddressRequest request, Long branchId) {

        Branch branch = branchRepository.findById(branchId).orElseThrow(() -> new RuntimeException(
                String.format("Branch not found with id -%s", branchId)
        ));


        Address address = modelMapper.map(request, Address.class);
        address.setBranch(branch);


        return modelMapper.map(addressRepository.save(address), AddressResponse.class);
    }

    public AddressResponse update(Long id, AddressRequest request) {
        Address address = addressRepository.findById(id).orElseThrow(() -> new RuntimeException(
                String.format("Address not found with id %s", id)
        ));

        modelMapper.map(request, address, "map");
        address.setId(id);

        return modelMapper.map(addressRepository.save(address), AddressResponse.class);
    }

    public void delete(Long id) {
        Address address = addressRepository.findById(id).orElseThrow(() -> new RuntimeException(
                String.format("Address not found with id %s", id)
        ));

        addressRepository.delete(address);
    }
}

